<?php
/**
 * File:  index.php
 * Creation Date: 19/09/2014
 * description:
 *
 * @author: canals
 */



//
// 1. Avant les autoloaders
//

/*
require_once 'src/personapp/personne/Personne.php';
require_once 'src/personapp/personne/Enseignant.php';
require_once 'src/personapp/personne/Etudiant.php';
require_once 'src/personapp/afficheur/AfficheurDePersonne.php';
require_once 'src/personapp/afficheur/AfficheurDEnseignant.php';
require_once 'src/personapp/afficheur/AfficheurDEtudiant.php';
 */


//
// 2. Avec la classe ClassLoader de l'exercice
//

/*
require_once 'conf/ClassLoader.php';


$loader = new \conf\ClassLoader('src');
$loader->register();
*/


//
// 3. Avec composer, méthode conventionnelle de fait
//

require_once 'vendor/autoload.php';

use \personapp\personne\Etudiant as Etudiant;

$p1 = new Etudiant('Jagger');


$p1->prenom='Mick'; $p1->age=23; $p1->noEtudiant = 435667;
$p1->refFormation='XDMA234'; $p1->groupe='2E';

if (isset( $_GET['jour'])) {
    echo "jour :" . $_GET['jour'] . " --> ". $p1->jourSemaine($_GET['jour']) . "<br>";
}

$p1->ajoutNote('math',10);
$p1->ajoutNote('math',12);$p1->ajoutNote('math',14);
$p1->ajoutNotes('info', '10;14.5;8;16');

echo "avg maths : ". $p1->moyenne('math'). '<br>';
echo "avg info : ". $p1->moyenne('info'). '<br>';
echo "avg expr : ". $p1->moyenne('expr'). '<br>';

var_dump($p1->moyenneGenerale());


$p2 = new \personapp\personne\Enseignant('Richards');
$p2->prenom='Keith'; $p2->age=22;
$p2->codeDiscipline = 27; $p2->composante='IUT-NC'; $p2->noBureau='0023';


$a = new \personapp\afficheur\AfficheurDEnseignant($p2);

$a->afficher('long');

$s = $p2->sommeAgeEnfants([10, 12, 14, 16]);

echo "somme age enfants : ". $s . '<br>';
