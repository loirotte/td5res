<?php
/**
 * File:  autoload.php
 * Creation Date: 21/09/2014
 * description:
 *
 * @author: canals
 */

namespace conf;

class ClassLoader {
    private $prefix ='';

    public function __construct($p) {
        $this->prefix = $p;
    }


    private function loadClass( $classname ) {

   //echo $classname ; echo '<br>';
    $filename= '';
    $classname = ltrim( $classname , '\\');

    $filename= str_replace ('\\', DIRECTORY_SEPARATOR, $classname);
    $filename .= '.php';
    $filename = $this->prefix . DIRECTORY_SEPARATOR . $filename;

    //echo $filename;
    if (is_file($filename)) include_once $filename;
    }

    public function register () {
        spl_autoload_register(array($this,'loadClass')) ;
    }
}

