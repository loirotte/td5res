<?php
/**
 * File:  Groupe.php
 * Creation Date: 06/04/2015
 * description:
 *
 * @author: canals
 */

namespace personapp\personne;


class Groupe {

    private $nomGrpe, $semestre, $formation;
    private $liste = array();

    public function __construct( $n, $s, $f) {
        $this->nomGrpe=$n;
        $this->semestre=$s;
        $this->formation=$f;
    }



    /**
     * __get : Magic getter
     *
     * @param string $attname nom de la propriété accédée
     * @return mixed valeur de la propriété pour l'objet courant
     * @access public
     */
    public function __get( $attname ) {
        if (property_exists($this, $attname)) return $this->$attname ;
    }

    /**
     * __set : magic setter
     *
     * @param string $attname nom de la propriété accédée
     * @param mixed $attval nouvelle valeur de la propriété accédée
     * @return mixed valeur de la propriété accédée
     * @access public
     */
    public function __set( $attname, $attval){
        if (property_exists($this, $attname)) {
            $this->$attname = $attval ;
            return $this->$attname ;
        }
    }

    /**
     * addEtudiant : ajoute un étudiant à les liste du groupe
     * @param Etudiant $e
     * @access public
     * @return void
     */
    public function addEtudiant ( \personapp\personne\Etudiant $e) {
        array_push($this->liste, $e);
    }

    /**
     * getListeEtudiant : retourne la liste des étudiants du groupe
     *
     * @return array : la liste des étudiants du groupe
     * @access public
     */
    public function getListeEtudiant() {
        return $this->liste;
    }

}