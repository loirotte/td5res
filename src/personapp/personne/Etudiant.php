<?php
/**
 * File:  Etudiant.php
 * Creation Date: 19/09/2014
 * description:
 *
 * @author: canals
 */

namespace personapp\personne ;

class Etudiant extends Personne{
    protected $noEtudiant, $refFormation, $groupe;
    private $notes = array();


    /**
     * punition  : pour les étudiants uniquement
     *
     * permet d'écrire ses lignes de punition
     *
     * @param int $n Le nombre de lignes à répéter
     * @param string $l la ligne à recopier
     * @access public
     * @return void
     */
    public function punition($n, $l) {
        for ($i=1 ; $i<=$n ; $i++) print $l.'<br>';
    }

    /**
     * jourSemaine : convertit un n° de jour en nom de jour et inversement
     *
     * @param $jour
     * @return mixed
     */
    public function jourSemaine($jour) {
        $t=[ 1=>"lundi", 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche',
            "lundi"=>1, 'mardi'=>2, 'mercredi'=>3, 'jeudi'=>4, 'vendredi'=>5, 'samedi'=>6, 'dimanche'=>7
           ];
        if (! array_key_exists($jour, $t)) return -1;
        return $t[$jour];
    }

    /**
     * ajoutNote : ajoute 1 note dans une matière à l'étudiant
     * @param $mat : la matière concernée
     * @param $note : la note à ajouter
     * @return void
     * @access public
     */
    public function ajoutNote( $mat, $note) {

        $this->notes[$mat][]=$note ;
    }

    /**
     * mouenne : calcule la moyenne des notes de l'étudiant dans une matière
     *
     * @param $mat : la matière pour laquelle on demande la moyenne
     * @return float|int : -1 sir la matière n'existe pas, la moyenne des npes
     * @access public
     */
    public function moyenne( $mat ){
        if (! array_key_exists($mat, $this->notes)) return -1;

        $s = 0;
        foreach ($this->notes[$mat] as $n) $s+=$n;
        return round($s / count($this->notes[$mat]),2);
    }

    /**
     * ajoutNotes : ajoute plusieurs notes pour 1 matière en 1 seule fois
     *
     * @param $mat : la matière concernée
     * @param $notes : les notes sous la forme d'une string avec ; comme séparateur
     * @access public
     * @return void
     */
    public function ajoutNotes($mat, $notes) {
        $t = explode(';', $notes);
        foreach ($t as $n) $this->ajoutNote($mat,$n);
    }

    /**
     * moyenneGenerale : calcul la moyenne générale de l'étudiant et retourne un tableau
     *                  contenant la moyenne générale et la moyenne de chaque matière
     * @return array : tableau contenant la moyenne générale et la moyenne par matière
     * @access public
     */
    public function moyenneGenerale() {
        $moy = array();
        $mg=0;
        foreach ($this->notes as $mat=>$n) {
            $moy[$mat] = $this->moyenne($mat);
            $mg+=$moy[$mat];
        }
        $moy['generale'] = round( $mg / count($this->notes),2);

        return $moy;

    }



} 