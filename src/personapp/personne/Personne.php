<?php
/**
 * File:  Personne.php
 * Creation Date: 19/09/2014
 * description:
 *
 * @author: canals
 */
namespace personapp\personne;
/**
 * Class Personne
 *
 * La classe Personne représente .... des Personnes !
 *
 * @author canals
 * @abstract
 */
abstract class Personne {

    protected $nom, $prenom, $age, $adr, $ville, $codep, $mail, $mobile, $idSkype ;
    protected $conjoint = null;

    /**
     * __construct : constructeur de personne
     *
     * @param string $nom le nom de la personne
     * @access public
     * @return Personne une nouvelle personne initialisée
     */
    public function __construct ( $nom ) {
        $this->nom = $nom ;
    }

    /**
     * __toString : Magic pour l'affichage
     *
     * retourne une chaîne JSON représentant l'objet
     *
     * @access public
     * @return string
     */
    public function __toString() {

        return '{   "nom" :"' . $this->nom .'",
                    "prenom" : "' . $this->prenom .'",
                    "adr" : "' . $this->adr .'",
                    "ville" :"' . $this->ville .'",
                    "codep" :"' . $this->codep .'",
                    "mail" :"' . $this->mail .'",
                    "mobile" :"' . $this->mobile .'",
                    "idSkype" :"' . $this->idSkype .'" }' ;

    }

    /**
     * __get : Magic getter
     *
     * @param string $attname nom de la propriété accédée
     * @return mixed valeur de la propriété pour l'objet courant
     * @access public
     */
    public function __get( $attname ) {
        if (property_exists($this, $attname)) return $this->$attname ;
    }

    /**
     * __set : magic setter
     *
     * @param string $attname nom de la propriété accédée
     * @param mixed $attval nouvelle valeur de la propriété accédée
     * @return mixed valeur de la propriété accédée
     * @access public
     */
    public function __set( $attname, $attval){
        if (property_exists($this, $attname)) {
            $this->$attname = $attval ;
            return $this->$attname ;
        }
    }
    /**
     * compter :
     *
     * la personne compte de 0 à son age et de son age à 0
     *
     * @access public
     * @return void
     */
    public function compter() {
        $i=0;

        for ($i=0; $i <= $this->age; $i++) print $i . '<br>';
        while ($i>=0) print $i-- . '<br>';
    }



    /**
     * ageN : calcul d'age
     *
     * Calcule et retourne l'age de la personne à l'année N
     *
     * @param int $n année pour laquelle on calcule
     * @return int age de la personne à l'année $n
     */
    public function ageN( $n ) {
        $anneNaiss = intval( date('Y')) - $this->age;
        return $n - $anneNaiss ;
    }



} 