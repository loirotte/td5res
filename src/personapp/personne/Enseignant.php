<?php
/**
 * File:  Enseignant.php
 * Creation Date: 19/09/2014
 * description:
 *
 * @author: canals
 */
namespace personapp\personne;


class Enseignant extends Personne{

    protected $codeDiscipline, $composante, $noBureau;


    /**
     * ajouterConjoint
     *
     * Permet de définir le conjoint d'une personne
     *
     * @param Personne $p
     * @access publi
     * @return void
     */
    public function ajouterConjoint( Personne $p) {
        $this->conjoint = $p;
    }

    /**
     * sommeAgeEnfants
     *
     * calcul la somme de l'age des enfants de l'enseigant
     *
     * @param Array $ages : tableau des ages des enfants de l'enseigant
     * @access public
     * @return integer : la somme
     */
    public function sommeAgeEnfants( Array $ages) {
        $somme=0;
        foreach ($ages as $age) $somme += $age;
        return $somme;
    }
} 