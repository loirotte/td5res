<?php
/**
 * File:  AfficheurDEtudiant.php
 * Creation Date: 21/09/2014
 * description:
 *
 * @author: canals
 */
namespace personapp\afficheur;

class AfficheurDEtudiant extends AfficheurDePersonne {


    public function __construct( \personapp\personne\Etudiant $p) {

        $this->p = $p;

    }



    public function vueDetail() {

        $html = '<div>' . '<h3>'. $this->p->nom .' '.$this->p->prenom .'</h3>'.
            '<h4>n° etudiant: '.$this->p->noEtudiant.' </h4>'.
            '<h4>ref. formation : '.$this->p->refFormation .' </h4>'.
            '<h4>n° groupe: '.$this->p->groupe.' </h4>'.
            '<h4>'.$this->p->age.'ans </h4>'.
            '<h4>'.$this->p->adr.'</h4>'.
            '<h4>'.$this->p->codep. ' '.$this->p->ville.'</h4>'.
            '<h4>'.$this->p->mail .'</h4>'.
            '<h4>'.$this->p->mobile .'</h4>'.
            '<h4>'.$this->p->idSkype .'</h4>'.
            '</div>';


        return $html;

    }

} 