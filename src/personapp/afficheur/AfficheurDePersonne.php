<?php
/**
 * File:  AfficheurDePersonne.php
 * Creation Date: 19/09/2014
 * description:
 *
 * @author: canals
 */
namespace personapp\afficheur;


abstract class AfficheurDePersonne {
    public $p ;

    public function __construct( \personapp\personne\Personne $p) {

        $this->p = $p;

    }


    public function vueCourte() {

        return '<div>' . '<h3>'. $this->p->nom .' '.$this->p->prenom .'</h3>'.
               '<h4>'.$this->p->ville.'</h4>'.
                '</div>';

    }

    public abstract function vueDetail() ;

    public function afficher ($sel) {
        $content=null;

        switch ($sel) {
            case 'short': {
                $content = $this->vueCourte();
                break;
            }
            case 'long': {
                $content = $this->vueDetail();
                break;
            }
        }

        echo <<<END
<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
$content
</body>
</html>
END;

    }
} 